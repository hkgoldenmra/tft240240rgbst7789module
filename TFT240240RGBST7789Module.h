#ifndef TFT240240RGBST7789MODULE_H
#define TFT240240RGBST7789MODULE_H

#include <Arduino.h>
#include <SPI.h>

class TFT240240RGBST7789Module {
	private:
		byte resPin;
		byte dcPin;
		byte csPin;
		byte sdaPin;
		byte sclPin;
		byte ramBits;
		byte colorBits;
		unsigned int line = 0;
		unsigned int scrollY = 0;
		unsigned int scrollH = 320;
		bool isDefaultSPI();
		void sendByte(byte);
	public:
		//
		static const byte BIT_12 = 0x03;
		static const byte BIT_16 = 0x05;
		static const byte BIT_18 = 0x06;
		static const byte BIT_24 = 0x07;
		//
		TFT240240RGBST7789Module(byte, byte, byte, byte, byte);
		TFT240240RGBST7789Module(byte, byte, byte);
		void initial();
		void hardwareReset();
		void softwareReset();
		void setSleepMode(bool);
		void setPartialMode(bool);
		void setInverseColor(bool);
		void setGamma(byte);
		void setDisplay(bool);
		void fillRect(unsigned int, unsigned int, unsigned int, unsigned int, const unsigned int);
		void setPixel(unsigned int, unsigned int, const unsigned int);
		void drawRect(unsigned int, unsigned int, unsigned int, unsigned int, const unsigned int);
		void setTearingEffect(bool);
		void setScrollLine(unsigned int);
		void setScrollArea(unsigned int, unsigned int);
		void setIdleMode(bool);
		void setColorMode(byte);
		void setColorMode(byte, byte);
		void setBrightness(byte);
};

#endif