#include <TFT240240RGBST7789Module.h>

bool TFT240240RGBST7789Module::isDefaultSPI() {
	return !((this->sdaPin != MOSI) || (this->sclPin != SCK));
}

void TFT240240RGBST7789Module::sendByte(byte data) {
	if (this->isDefaultSPI()) {
		SPI.transfer(data);
	} else {
		for (byte i = 0; i < 8; i++, data <<= 1) {
			digitalWrite(this->sclPin, LOW);
			digitalWrite(this->sdaPin, (data & 0x80) > 0);
			digitalWrite(this->sclPin, HIGH);
		}
	}
}

TFT240240RGBST7789Module::TFT240240RGBST7789Module(byte resPin, byte dcPin, byte csPin, byte sdaPin, byte sclPin) {
	this->resPin = resPin;
	this->dcPin = dcPin;
	this->csPin = csPin;
	this->sdaPin = sdaPin;
	this->sclPin = sclPin;
}

TFT240240RGBST7789Module::TFT240240RGBST7789Module(byte resPin, byte dcPin, byte csPin)
: TFT240240RGBST7789Module::TFT240240RGBST7789Module(resPin, dcPin, csPin, MOSI, SCK) {
}

void TFT240240RGBST7789Module::initial() {
	pinMode(this->resPin, OUTPUT);
	digitalWrite(this->resPin, HIGH);
	pinMode(this->csPin, OUTPUT);
	digitalWrite(this->csPin, HIGH);
	pinMode(this->dcPin, OUTPUT);
	pinMode(this->sdaPin, OUTPUT);
	pinMode(this->sclPin, OUTPUT);
	if (this->isDefaultSPI()) {
		SPI.beginTransaction(SPISettings(32000000, MSBFIRST, SPI_MODE0));
		SPI.begin();
	}
}

void TFT240240RGBST7789Module::hardwareReset() {
	digitalWrite(this->resPin, LOW);
	digitalWrite(this->resPin, HIGH);
}

void TFT240240RGBST7789Module::softwareReset() {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x01);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setSleepMode(bool data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x10 | !data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setPartialMode(bool data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x12 | !data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setInverseColor(bool data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x20 | !data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setGamma(byte data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x26);
	this->sendByte(data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setDisplay(bool data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x28 | data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::fillRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned int c) {
	w += x - 1;
	h += y - 1;
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x2A);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte((x >> 8) & 0xFF);
	this->sendByte(x & 0xFF);
	this->sendByte((w >> 8) & 0xFF);
	this->sendByte(w & 0xFF);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x2B);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte((y >> 8) & 0xFF);
	this->sendByte(y & 0xFF);
	this->sendByte((h >> 8) & 0xFF);
	this->sendByte(h & 0xFF);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x2C);
	digitalWrite(this->dcPin, HIGH);
	for (byte j = 0; j <= h; j++) {
		for (byte i = 0; i <= w; i++) {
			this->sendByte((c >> 8) & 0xFF);
			this->sendByte(c & 0xFF);
		}
	}
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setPixel(unsigned int x, unsigned int y, const unsigned int c) {
	this->fillRect(x, y, 1, 1, c);
}

void TFT240240RGBST7789Module::drawRect(unsigned int x, unsigned int y, unsigned int w, unsigned int h, const unsigned int c) {
	this->fillRect(x, y, 1, h, c);
	this->fillRect(x, y, w, 1, c);
	this->fillRect(w + x, y, 1, h, c);
	this->fillRect(x, h + y, w, 1, c);
}

void TFT240240RGBST7789Module::setTearingEffect(bool data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x34 | data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setScrollArea(unsigned int y, unsigned int h) {
	unsigned int b = 320 - (this->scrollY = y) - (this->scrollH = h);
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x33);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte((y >> 8) & 0xFF);
	this->sendByte(y & 0xFF);
	this->sendByte((h >> 8) & 0xFF);
	this->sendByte(h & 0xFF);
	this->sendByte((b >> 8) & 0xFF);
	this->sendByte(b & 0xFF);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setScrollLine(unsigned int data) {
	data %= this->scrollH;
	data += this->scrollY;
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x37);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte((data >> 8) & 0xFF);
	this->sendByte(data & 0xFF);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setIdleMode(bool data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x38 | data);
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setColorMode(byte ramBits, byte colorBits) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, LOW);
	this->sendByte(0x3A);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte(((this->ramBits = ramBits) << 8) | (this->colorBits = colorBits & 0x07));
	digitalWrite(this->csPin, HIGH);
}

void TFT240240RGBST7789Module::setColorMode(byte data) {
	this->setColorMode(data, data);
}

void TFT240240RGBST7789Module::setBrightness(byte data) {
	digitalWrite(this->csPin, LOW);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte(0x51);
	digitalWrite(this->dcPin, HIGH);
	this->sendByte(data);
	digitalWrite(this->csPin, HIGH);
}