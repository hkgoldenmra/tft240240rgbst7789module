#include <TFT240240RGBST7789Module.h>

const byte dcPin = 8;
const byte resPin = 9;
const byte csPin = 10;
const byte sdaPin = 11;
const byte sclPin = 13;
unsigned int l = 0;

TFT240240RGBST7789Module module = TFT240240RGBST7789Module(resPin, dcPin, csPin, sdaPin, sclPin);

void setup() {
	Serial.begin(9600);
	module.initial();
	module.setScrollLine(0);
	module.setScrollArea(0, 320);
	module.setColorMode(TFT240240RGBST7789Module::BIT_16);
	module.setInverseColor(false);
	module.setSleepMode(false);
	module.setDisplay(true);
	module.fillRect(0, 0, 240, 240, 0x0000);
	module.fillRect(0, 0, 240, 60, 0xF800);
	module.fillRect(0, 60, 240, 60, 0x7E0);
	module.fillRect(0, 120, 240, 60, 0x001F);
	module.fillRect(0, 180, 240, 60, 0xFFFF);
	module.setScrollArea(60, 120);
}

void loop() {
	normal();
}

void normal() {
	l += 1;
	module.setScrollLine(l);
	delay(10);
}